use num_base::{ Based, Alphabet };

#[test]
fn conversion_test() {
    let c1 = Based::new("101", 10).to(2).unwrap();
    assert_eq!(c1.val, "1100101");
    assert_eq!(c1.base, 2);

    let c2 = Based::new("1100101", 2).to(10).unwrap();
    assert_eq!(c2.val, "101");
    assert_eq!(c2.base, 10);

    let c3 = Based::new("1210", 3).to(2).unwrap();
    assert_eq!(c3.val, "110000");
    assert_eq!(c3.base, 2);

    let c4 = Based::new_abc("bof", 16, "abcdefghijklmnopqrstuvwxyz").to(8).unwrap();
    assert_eq!(c4.val, "hef");
    assert_eq!(c4.base, 8);

    let c5 = Based::new_abc("bababa", 2, "abcde").to(3).unwrap().convert_to_abc(Alphabet::Default).unwrap();
    assert_eq!(c5.val, "1120");
    assert_eq!(c5.base, 3);
}

#[cfg(feature = "ops")]
#[test]
fn ops() {
    assert_eq!((Based::new("101", 10) + Based::new("1100101", 2)).val, "202");
    assert_eq!((Based::new("101", 10) - Based::new("1100100", 2)).val, "1");
    assert_eq!((Based::new("101", 10) * Based::new("10", 2)).val, "202");
    assert_eq!((Based::new("100", 10) / Based::new("10", 2)).val, "50");
    assert_eq!((Based::new("101", 10) % Based::new("10", 2)).val, "1");
}