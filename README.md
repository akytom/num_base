# num_base

Crate for manipulating with numbers (integers) in different bases.

# Quick start

```rust
use num_base::Based;

let num = Based::new("101", 10).to(2);

assert_eq!(num.val, "1100101")
```

# Optional features
- **`ops`** - Implementation for Add, Sub, Mul, Div and Rem.
- **`cli`** - Install with: ```cargo install num_base --features cli```.