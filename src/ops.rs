use crate::{ Alphabet, Based };
use std::ops::{ Add, Sub, Mul, Div, Rem };

impl Add for Based {
    type Output = Self;

    fn add(self, other: Self) -> Self {
        Self {
            val: (self.value().unwrap() + other.value().unwrap()).to_string(),
            base: 10,
            alphabet: Alphabet::Default,
        }
    }
}

impl Sub for Based {
    type Output = Self;

    fn sub(self, other: Self) -> Self {
        Self {
            val: (self.value().unwrap() - other.value().unwrap()).to_string(),
            base: 10,
            alphabet: Alphabet::Default,
        }
    }
}

impl Mul for Based {
    type Output = Self;

    fn mul(self, other: Self) -> Self {
        Self {
            val: (self.value().unwrap() * other.value().unwrap()).to_string(),
            base: 10,
            alphabet: Alphabet::Default,
        }
    }
}

impl Div for Based {
    type Output = Self;

    fn div(self, other: Self) -> Self {
        Self {
            val: (self.value().unwrap() / other.value().unwrap()).to_string(),
            base: 10,
            alphabet: Alphabet::Default,
        }
    }
}

impl Rem for Based {
    type Output = Self;

    fn rem(self, other: Self) -> Self {
        Self {
            val: (self.value().unwrap() % other.value().unwrap()).to_string(),
            base: 10,
            alphabet: Alphabet::Default,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn ops() {
        assert_eq!(Based::new("101", 10) + Based::new("1100101", 2), Based::new("11001010", 2));
        assert_eq!(Based::new("101", 10) - Based::new("1100100", 2), Based::new("1", 2));
        assert_eq!(Based::new("101", 10) * Based::new("10", 2), Based::new("312", 8));
        assert_eq!(Based::new("100", 10) / Based::new("10", 2), Based::new("50", 10));
        assert_eq!(Based::new("101", 10) % Based::new("10", 2), Based::new("1", 32));
    }
}