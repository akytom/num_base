use clap::Parser;
use num_base::{ Based, Alphabet };

#[derive(clap::Parser, Debug)]
#[clap(author = "akytom", version, about = "CLI for number base conversion.")]
struct Cli {
    /// Number to be converted
    number: String,

    /// Base of the input number
    #[clap(short, long, value_parser, default_value_t = 10)]
    from: usize,

    /// Alphabet of the input number
    #[clap(short, long, value_parser, default_value_t = Alphabet::Default)]
    alphabet: Alphabet,

    /// Base of the output number
    #[clap(short, long, value_parser, default_value_t = 10)]
    to: usize,
}

fn main() {
    let args = Cli::parse();

    if args.from == args.to { print!("Base from and base to are the same! => ") }
    let num = Based::new_abc(&args.number, args.from, args.alphabet.simplify());

    println!("{}", num.to(args.to).unwrap().val)
}