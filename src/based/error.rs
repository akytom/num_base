use crate::AlphabetError;

/// Based number's Error.
#[derive(Debug, PartialEq)]
pub enum BasedError {
    /// Couldn't parse ([ParseIntError](std::num::ParseIntError)).
    ParseInt,
    /// Number with base 0.
    BaseZero,
    /// Error with Alphabet.
    Alphabet(AlphabetError),
}

impl From<core::num::ParseIntError> for BasedError {
    fn from(_err: core::num::ParseIntError) -> Self {
        Self::ParseInt
    }
}

impl From<AlphabetError> for BasedError {
    fn from(err: AlphabetError) -> Self {
        Self::Alphabet(err)
    }
}