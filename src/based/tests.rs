use crate::Alphabet;
use super::*;

#[test]
fn new() {
    let n1 = Based::new("101", 10);
    assert_eq!(n1.val, "101");
    assert_eq!(n1.base, 10);
    assert_eq!(n1.alphabet, Alphabet::Default);

    let n2 = Based::new("1100101", 2);
    assert_eq!(n2.val, "1100101");
    assert_eq!(n2.base, 2);
    assert_eq!(n2.alphabet, Alphabet::Default);

    let n3 = Based::new_unchecked("123", 3);
    assert_eq!(n3.val, "123");
    assert_eq!(n3.base, 3);
    assert_eq!(n3.alphabet, Alphabet::Default);
    assert!(!n3.is_valid());

    let n4 = Based::new_abc("bof", 16, Alphabet::Latin);
    assert_eq!(n4.val, "bof");
    assert_eq!(n4.base, 16);
    assert_eq!(n4.alphabet, Alphabet::Latin);
}

#[test]
fn conversion_test() {
    let c1 = Based::new("101", 10).to(2).unwrap();
    assert_eq!(c1.val, "1100101");
    assert_eq!(c1.base, 2);
    assert_eq!(c1.alphabet, Alphabet::Default);
    assert!(c1.is_valid());

    let c2 = Based::new("1100101", 2).to(10).unwrap();
    assert_eq!(c2.val, "101");
    assert_eq!(c2.base, 10);
    assert_eq!(c2.alphabet, Alphabet::Default);
    assert!(c2.is_valid());

    let c3 = Based::new("1210", 3).to(2).unwrap();
    assert_eq!(c3.val, "110000");
    assert_eq!(c3.base, 2);
    assert_eq!(c3.alphabet, Alphabet::Default);
    assert!(c3.is_valid());

    let c4 = Based::new_abc("bof", 16, Alphabet::Latin).to(8).unwrap();
    assert_eq!(c4.val, "hef");
    assert_eq!(c4.base, 8);
    assert_eq!(c4.alphabet, Alphabet::Latin);
    assert!(c4.is_valid());

    let c5 = Based::new_abc("bababa", 2, Alphabet::Latin).to(3).unwrap().convert_to_abc(Alphabet::Default).unwrap();
    assert_eq!(c5.val, "1120");
    assert_eq!(c5.base, 3);
    assert_eq!(c5.alphabet, Alphabet::Default);
    assert!(c5.is_valid());
}

#[test]
fn value() {
    let n1 = Based::new("1100101", 2).value().unwrap();

    assert_eq!(n1, 101);
}

#[test]
fn convert_abc() {
    let n1 = Based::new("1100101", 2).convert_to_abc(Alphabet::Latin).unwrap();

    assert_eq!(n1.val, "bbaabab");
}

#[test]
fn check() {
    let n1 = Based::new_unchecked("101", 0);
    let n2 = Based::new_unchecked("101", 1);
    let n3 = Based::new_unchecked("101", 10);

    assert_eq!(n1.check(), Err( BasedError::BaseZero ));
    assert_eq!(n2.check(), Err( BasedError::Alphabet(AlphabetError::TooHighValue('1')) ));
    assert_eq!(n3.check(), Ok( Based::new("101", 10) ))
}

#[test]
fn is_valid() {
    let n1 = Based::new_unchecked("101", 0);
    let n2 = Based::new_unchecked("101", 1);
    let n3 = Based::new_unchecked("101", 10);

    assert!(!n1.is_valid());
    assert!(!n2.is_valid());
    assert!(n3.is_valid())
}

#[test]
fn builder() {
    let num = Based::builder().val("101").base(10).build().to(2).unwrap();

    assert_eq!(num.val, "1100101")
}