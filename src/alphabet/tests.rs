use super::{ Alphabet, AlphabetError, DEFAULT, UPPERCASE, DECIMAL, LATIN, LATIN_UPPERCASE };

#[test]
fn is_valid() {
    let alphabet1 = Alphabet::from("0123456789");
    let alphabet2 = Alphabet::from("0120");

    assert!(alphabet1.is_valid());
    assert!(!alphabet2.is_valid());
}

#[test]
fn to_valid() {
    let alphabet1 = Alphabet::from("0120");
    let alphabet2 = Alphabet::from("012345");

    assert_eq!(alphabet1.to_valid().get(), "012");
    assert_eq!(alphabet2.to_valid().get(), "012345");
}

#[test]
fn check() {
    let alphabet1 = Alphabet::from("0120");
    let alphabet2 = Alphabet::from("012345");

    assert_eq!(alphabet1.check(), Err(AlphabetError::RepeatedCharacters));
    assert_eq!(alphabet2.check(), Ok(Alphabet::from("012345")));
}

#[test]
fn get() {
    let default = Alphabet::Default;
    let decimal = Alphabet::Decimal;
    let custom = Alphabet::new("abcdefghijklmnopqrstuvwxyz");

    assert_eq!(default.get(), DEFAULT);
    assert_eq!(decimal.get(), DECIMAL);
    assert_eq!(custom.get(), "abcdefghijklmnopqrstuvwxyz");
}

#[test]
fn simplify() {
    let default = Alphabet::new(DEFAULT).simplify();
    let uppercase = Alphabet::new(UPPERCASE).simplify();
    let decimal = Alphabet::new(DECIMAL).simplify();
    let latin = Alphabet::new(LATIN).simplify();
    let latin_uppercase = Alphabet::new(LATIN_UPPERCASE).simplify();
    let custom = Alphabet::new("142857").simplify();

    assert_eq!(default, Alphabet::Default);
    assert_eq!(uppercase, Alphabet::UpperCase);
    assert_eq!(decimal, Alphabet::Decimal);
    assert_eq!(latin, Alphabet::Latin);
    assert_eq!(latin_uppercase, Alphabet::LatinUpperCase);
    assert_eq!(custom, Alphabet::new("142857"));
}

#[test]
fn nth_char() {
    let default = Alphabet::Default;

    assert_eq!(default.nth_char(5), Ok('5'));
    assert_eq!(default.nth_char(16), Ok('g'));

    assert_eq!(Alphabet::new("abcd").nth_char(3), Ok('d'));
    assert_eq!(Alphabet::new("あいうえお").nth_char(1), Ok('い'));
    assert_eq!(Alphabet::new("花鳥風月").nth_char(2), Ok('風'));
}

#[test]
fn chars_value() {
    let default = Alphabet::Default;

    assert_eq!(default.chars_index('5'), Ok(5));
    assert_eq!(default.chars_index('g'), Ok(16));

    assert_eq!(Alphabet::new("abcd").chars_index('d'), Ok(3));
    assert_eq!(Alphabet::new("あいうえお").chars_index('い'), Ok(1));
    assert_eq!(Alphabet::new("花鳥風月").chars_index('風'), Ok(2));
}