/// Alphabet's Error.
#[derive(Debug, PartialEq)]
pub enum AlphabetError {
    /// Alphabet contains repeated characters *(see: [Alphabet::to_valid()](crate::Alphabet::to_valid()))*.
    RepeatedCharacters,
    /// Alphabet is too small.
    TooSmallAlphabet(usize),
    /// Character not found.
    CharacterNotIncluded(char),
    /// Character's value is higher than the base.
    TooHighValue(char)
}

#[cfg(feature = "cli")]
use crate::Alphabet;

#[cfg(feature = "cli")]
use std::fmt;

#[cfg(feature = "cli")]
impl fmt::Display for Alphabet {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.get())
    }
}

#[cfg(feature = "cli")]
impl std::str::FromStr for Alphabet {
    type Err = std::string::ParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(Self::new(s))
    }
}