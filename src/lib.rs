//! Crate for manipulating with numbers (integers) in different bases.
//!
//! # Quick start
//!
//! ```rust
//! use num_base::Based;
//!
//! let num = Based::new("101", 10).to(2).unwrap();
//!
//! assert_eq!(num.val, "1100101")
//! ```
//!
//! # Optional features
//! - **`ops`** - Implementation for Add, Sub, Mul, Div and Rem.
//! - **`cli`** - Install with: ```cargo install num_base --features cli```.

#![warn(missing_docs)]

#[cfg(feature = "ops")] mod ops;
mod alphabet;
mod based;

#[doc(inline)]
pub use alphabet::Alphabet;
pub use alphabet::AlphabetError;

pub use based::Based;
pub use based::BasedError;
pub use based::BasedBuilder;